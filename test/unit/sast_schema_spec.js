import b from './builders/index'
import {schemas} from './support/schemas'

describe('sast schema', () => {

  it('should validate location', () => {
    const report = b.sast.report({
      vulnerabilities: [b.sast.vulnerability({
        location: {
          file: 'Main.java',
          class: 'Main',
          method: 'main',
          start_line: 10,
          end_line: 20
        }
      })]
    })

    expect(schemas.sast.validate(report).success).toBeTruthy()
  })

})
