import {merge} from '../../support/merge'

const defaults = {
  analyzer: {
    id: 'test_analyzer_id',
    name: 'Test Analyzer name',
    vendor: {
      name: "Test Vendor"
    },
    version:"0.1.0"
  },
  scanner: {
    id: 'starboard_trivy',
    name: 'Trivy (via Starboard Operator)',
    vendor: {
      name: 'GitLab'
    },
    version: '0.19.2'
  },
  type: 'cluster_image_scanning',
  status: 'success',
  start_time: '2020-09-18T20:06:51',
  end_time: '2020-09-18T20:07:01'
}

export const scan = (scanOverrides) => {
  return merge(defaults, scanOverrides)
}
